import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { Router } from '@angular/router';
import * as papa from 'papaparse';
import { Storage } from '@ionic/storage';
import { Dashboard } from '../print/print.page'
 

const ITEMS_KEY = 'dashboard';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  csvData: any[] = [["V"],["R"]];
  headerRow: any[] = ["N"];
  
  date: any;

  constructor(private menu: MenuController, private router: Router,private storage: Storage) { 
    this.date=new Date();
  }

  order_count: number;
  total_sales: number;
  ngOnInit() {
    
  }

  ionViewWillEnter(){
    this.storage.get(ITEMS_KEY).then(data => {
      if(data){
        this.order_count = data.order_count;
        this.total_sales = data.total_sales;
      }else{
        this.order_count = 0;
        this.total_sales = 0;
      }
    });
  }


  openFirst() {
    this.menu.enable(true, 'first');
    this.menu.open('first');
  }

  openEnd() {
    this.menu.open('end');
  }
  openCustom() {
    this.menu.enable(true, 'custom');
    this.menu.open('custom');
  }
  order(){
    this.router.navigateByUrl('/menu-list');
  }

  downloadCSV() {
    let csv = papa.unparse({
      fields: this.headerRow,
      data: this.csvData
    });
    console.log(csv);
    var blob = new Blob([csv]);
    var a = window.document.createElement("a");
    a.href = window.URL.createObjectURL(blob);
    a.download = "newdata.csv";
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
  }

}
