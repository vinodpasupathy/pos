import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Storage } from '@ionic/storage';
import { Cart } from '../menu-list/menu-list.page';

export interface Customer {
id: number, 
customer_name: string, 
mobile_no: number, 
payment_type: string, 
amount: number,
}

const ITEMS_KEY = 'customer';

const CART_ITEMS_KEY = 'cart';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.page.html',
  styleUrls: ['./payment.page.scss'],
})
export class PaymentPage implements OnInit {

  paymentForm : FormGroup;

  customer: Customer[]= [];

  constructor(private router: Router,private formBuilder: FormBuilder,private storage: Storage) { }

  ngOnInit() {
    this.paymentForm = this.formBuilder.group({
      customer_name: ['', Validators.required],
      mobile_no: ['', Validators.required],
      payment_type: ['', Validators.required],
      amount: ['', Validators.required]
  });
  this.storage.get(CART_ITEMS_KEY).then((carts: Cart[]) => {
    this.total(carts);
  });
     
  }

  sum: number = 0;

  total(cart){
    for (let i of cart) {
      if (i.unit_price) {
        this.sum = this.sum + (i.unit_price * i.quantity);
      }
    }
  }

  back(){
    this.router.navigateByUrl('/orders');
  }
  print(){
    console.log(this.paymentForm.value);

    /*this.storage.get(ITEMS_KEY).then((carts: Customer[]) => {
      console.log(carts);
      if (!carts) {
        this.customer.push(this.paymentForm.value);
        this.storage.set(ITEMS_KEY, this.customer);
      } else {
        this.storage.set(ITEMS_KEY, [this.paymentForm.value]);
      }
    });*/
    this.router.navigateByUrl('/print');
  }
}
