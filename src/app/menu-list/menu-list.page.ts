import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { of, from } from 'rxjs';
import { pluck, toArray, reduce } from 'rxjs/operators';

export interface Item {
id: number, 
item_name: string, 
item_description: string, 
unit_price: number, 
category: string,
unit: string,
active: boolean,
user_id: number,
dummy_cart: number,
created_at: Date;
}

​export interface Cart {
  id: number, 
  item_name: string, 
  unit_price: number, 
  category: string,
  unit: string,
  user_id: number,
  quantity: number,
  created_at: Date;
}


const ITEMS_KEY = 'item';

const CART_ITEMS_KEY = 'cart';

@Component({
  selector: 'app-menu-list',
  templateUrl: './menu-list.page.html',
  styleUrls: ['./menu-list.page.scss'],
})
export class MenuListPage implements OnInit {
  data: Item[] = [{id: 1,item_name: "Chicken",item_description: "Non Veg",unit_price: 110,category: "starter",unit: "item",active: true,user_id: 1,created_at: new Date(),dummy_cart: 1},
  {id: 2,item_name: "Egg",item_description: "Startet",unit_price: 15,category: "starter",unit: "item",active: true,user_id: 1,created_at: new Date(),dummy_cart: 1},
  {id: 3,item_name: "Vanilla Ice cream",item_description: "Softies",unit_price: 60,category: "starter",unit: "item",active: true,user_id: 1,created_at: new Date(),dummy_cart: 1},
  {id: 4,item_name: "Idly",item_description: "Tiffen",unit_price: 30,category: "starter",unit: "item",active: true,user_id: 1,created_at: new Date(),dummy_cart: 1},
  {id: 5,item_name: "Masala Dosai",item_description: "Tiffin",unit_price: 60,category: "starter",unit: "item",active: true,user_id: 1,created_at: new Date(),dummy_cart: 1},
  {id: 6,item_name: "Veg Masala Gravy",item_description: "Gravy",unit_price: 120,category: "starter",unit: "item",active: true,user_id: 1,created_at: new Date(),dummy_cart: 1},
  {id: 7,item_name: "Chicken Lollipop",item_description: "Non Veg",unit_price: 180,category: "starter",unit: "item",active: true,user_id: 1,created_at: new Date(),dummy_cart: 1},
  {id: 8,item_name: "Clear Soup",item_description: "Appetizer",unit_price: 80,category: "starter",unit: "item",active: true,user_id: 1,created_at: new Date(),dummy_cart: 1},
  {id: 9,item_name: "Vegetable soup",item_description: "Appetizer",unit_price: 60,category: "starter",unit: "item",active: true,user_id: 1,created_at: new Date(),dummy_cart: 1}];

  items: Item[]= [];

  Cart: Cart[] = [];
  constructor(private router: Router,private storage: Storage) { }

  ngOnInit() {
    this.storage.set(ITEMS_KEY, this.data);
    if(this.storage.get(ITEMS_KEY)){
      this.get_items();
    }
  }

  get_items(){
    this.storage.get(ITEMS_KEY).then(data => {
      this.items = data;
      console.log(this.items);
    });
  }

  back(){
    this.router.navigateByUrl('/home');
  }

  add(i){
    this.items[i].dummy_cart = this.items[i].dummy_cart+1;
  }

  reduce(i){
    this.items[i].dummy_cart = this.items[i].dummy_cart-1;
  }
  
  AddToCart(datum){
   console.log(datum);
   //this.storage.set(ITEMS_KEY, this.data);
   let cart_data = { 
    "item_name": datum["item_name"], 
    "unit_price": datum["unit_price"], 
    "category": datum["category"],
    "unit": datum["unit"],
    "user_id": datum["user_id"],
    "quantity": datum["dummy_cart"],
    "created_at": new Date()
  } 
  this.storage.get(CART_ITEMS_KEY).then((carts: Cart[]) => {
    if(carts){
    from(carts).pipe(pluck('item_name'),toArray()).subscribe(val=> {
      if (!val.includes(datum["item_name"])) {
        this.add_cart(cart_data);
      } else {
          alert("item already added to cart");
      } 
    });
  }else{
    this.add_cart(cart_data);
  }
  });
     

  }

  add_cart(cart_data){
    this.storage.get(CART_ITEMS_KEY).then((carts: Cart[]) => {
      console.log(carts);
      if (!carts) {
        this.Cart.push(cart_data);
        this.storage.set(CART_ITEMS_KEY, this.Cart);
      } else {
        this.Cart = carts;
        this.Cart.push(cart_data);
        this.storage.set(CART_ITEMS_KEY, this.Cart);
      }
    });
  }

  cart(){
    this.router.navigateByUrl('/orders');
  }
}
