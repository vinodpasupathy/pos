import { Component, OnInit } from '@angular/core';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Storage } from '@ionic/storage';

export interface User {
  id: number,
  username: string,
  password: string
}

const ITEMS_KEY = 'user';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  userForm : FormGroup;

  user: User[] = [];

  constructor(private statusBar: StatusBar, private router: Router,private formBuilder: FormBuilder,private storage: Storage) { }

  ngOnInit() {
    // this.statusBar.overlaysWebView(true);
    //this.statusBar.hide();
    this.userForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
  });
  this.storage.set(ITEMS_KEY, {id: 1,username: 'admin@gmail.com', password: 'admin'});
  }


  login(){
    let login = this.storage.get(ITEMS_KEY).then(data => {
      this.user = data;
    if(this.userForm.value.username == 'admin@gmail.com' && this.userForm.value.password == 'admin'){
    this.router.navigateByUrl('/home');
    }else{
        alert("login_failed");
    }
    });
    
  }
}
