import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { Cart } from '../menu-list/menu-list.page';
 

const ITEMS_KEY = 'cart';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.page.html',
  styleUrls: ['./orders.page.scss'],
})
export class OrdersPage implements OnInit {

  //data: any[] = [{id: 1,item_name: "a",item_descreption: "aa",unit_price: 120,category: "starter",unit: "item",active: "true",user_id: 1,created_at: "",dummy_cart: 1},
  //{id: 2,item_name: "b",item_descreption: "bb",unit_price: 190,category: "starter",unit: "item",active: "true",user_id: 1,created_at: "",dummy_cart: 1}];
   
  cart: Cart[] = [];

  constructor(private router: Router,private storage: Storage) { }

  ngOnInit() {

    this.storage.get(ITEMS_KEY).then(data => {
      if(data){
      this.cart = data;
      console.log(this.cart);
      this.total(this.cart);
      }
    });
  }

  add(index){
    this.cart[index].quantity = this.cart[index].quantity+1;


    this.storage.get(ITEMS_KEY).then((items: Cart[]) => {
      if (!items || items.length === 0) {
        
      }
 
      let newItems: Cart[] = [];
 
      for (let i of items) {
        if (i.item_name === this.cart[index].item_name) {
          newItems.push(this.cart[index]);
        }
      }
 
       this.storage.set(ITEMS_KEY, newItems);
    });

    //this.load_again()


  }

  reduce(index){
    this.cart[index].quantity = this.cart[index].quantity-1;


    this.storage.get(ITEMS_KEY).then((items: Cart[]) => {
      if (!items || items.length === 0) {
        
      }
 
      let newItems: Cart[] = [];
 
      for (let i of items) {
        if (i.item_name === this.cart[index].item_name) {
          newItems.push(this.cart[index]);
        }
      }
 
       this.storage.set(ITEMS_KEY, newItems);
    });

    //this.load_again()
  }

  remove_from_cart(datum,index){
   //console.log(datum);
           
     this.storage.get(ITEMS_KEY).then((items: Cart[]) => {
    if (!items || items.length === 0) {
      //alert("cart is empty");
    }

    let toKeep: Cart[] = [];

    for (let i of items) {
      if (i.item_name !== datum.item_name) {
        toKeep.push(i);
      }
    }
     this.storage.set(ITEMS_KEY, toKeep);
     this.cart.splice(index,1);
     this.sum = this.sum - (datum.unit_price * datum.quantity);
     if(toKeep){
     //this.load_again();
     }
  });
  }

  load_again(){
    this.ngOnInit();
  }

  sum: number = 0;

  total(cart){
    for (let i of cart) {
      if (i.unit_price) {
        this.sum = this.sum + (i.unit_price * i.quantity);
      }
    }
  }

  

  back(){
    this.router.navigateByUrl('/menu-list');
  }
  pay(){
    this.router.navigateByUrl('/payment');
  }
}
