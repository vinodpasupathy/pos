import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { Cart } from '../menu-list/menu-list.page';
import { of, from } from 'rxjs';
import { pluck, toArray, reduce } from 'rxjs/operators';
//import { Printer, PrintOptions } from '@ionic-native/printer/ngx';

export interface Dashboard {
 order_count: number;
 total_sales: number;
}


const ITEMS_KEY = 'cart';

const DASHBOARD_ITEMS_KEY = 'dashboard';

@Component({
  selector: 'app-print',
  templateUrl: './print.page.html',
  styleUrls: ['./print.page.scss'],
})
export class PrintPage implements OnInit {

  cart: Cart[] = [];

  constructor(private router: Router,private storage: Storage) { }

  ngOnInit() {
    this.storage.get(ITEMS_KEY).then(data => {
      this.cart = data;
      //const amount = from(this.cart).pipe(pluck('unit_price', 'quantity'),pluck('quantity')).subscribe(val => console.log(val));
      
      this.total(this.cart);
      console.log(this.cart);
    });
  }

  sum: number = 0;

  total(cart){
    for (let i of cart) {
      if (i.unit_price) {
        this.sum = this.sum + (i.unit_price * i.quantity);
      }
    }
  }

  print(){
   // this.storage.set(DASHBOARD_ITEMS_KEY, this.data);
    this.storage.get(DASHBOARD_ITEMS_KEY).then(data => {
      if(data){
        this.storage.set(DASHBOARD_ITEMS_KEY, { order_count: data.order_count + 1,total_sales: data.total_sales + this.sum});
      }else{
        this.storage.set(DASHBOARD_ITEMS_KEY, { order_count: 1,total_sales: this.sum});
      }
    })
    //this.printer.isAvailable().then(onSuccess, onError);
    this.storage.remove(ITEMS_KEY);
/*let options: PrintOptions = {
     name: 'MyDocument',
     printerId: 'printer007',
     duplex: true,
     landscape: true,
     grayscale: true
   }*/

//this.printer.print("content", options);//.then(onSuccess, onError){};

    this.router.navigateByUrl('/home');
  }
}
